package com.Employee;

public class EmployeeException extends Exception{
	public EmployeeException() {
		super("Invalid Employee Number");
	}
}
