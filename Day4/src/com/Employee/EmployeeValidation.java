package com.Employee;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class EmployeeValidation{
	public static void main(String[] args) throws EmployeeException {
		Scanner sc = new Scanner(System.in);
		File file = new File("employeedata.txt");
		try {
			FileOutputStream out = new FileOutputStream(file);
			System.out.println("Enter your EmployeeID");
			out.write(sc.next().getBytes());
			System.out.println("The employeeID was written to " + file);
			out.close();
			FileInputStream input = new FileInputStream(file);
			Scanner in = new Scanner(input);
			while(in.hasNext()) {
				if(in.nextLine().matches("[A-Z]{3}[0-9]{3}"))
					System.out.println("The EmployeeID provided is valid");
				else
					throw new EmployeeException();
			}
			
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("File wasn't found!");
		}
		catch(IOException e) {
			e.printStackTrace();
			System.out.println("IO Exception occured");
		}
		catch(EmployeeException e) {
			System.out.println(e.getMessage());
		}
	}
}
